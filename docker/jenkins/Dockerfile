ARG JENKINS_VERSION=2.305
FROM jenkins/jenkins:${JENKINS_VERSION} as base

ARG BUILD_DATE

LABEL maintainer="devops@acme.com" \
      description="This is a base jenkins master image, which allows connecting Jenkins agents via JNLP protocols" \
      vendor="ACME INC" \
      version="${JENKINS_VERSION}" \
      build-date="${BUILD_DATE}" \
      build-number="${BUILD_NUMBER}"

ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false" \
    COPY_REFERENCE_FILE_LOG="${JENKINS_HOME}/copy_reference_file.log" \
    BUILD_DATE="${BUILD_DATE}" \
    BUILD_NUMBER="${BUILD_NUMBER}" \
    JENKINS_VERSION="${JENKINS_VERSION}"

COPY scriptApproval.xml /usr/share/jenkins/ref/scriptApproval.xml
COPY list_tags.sh /var/lib/jenkins/bin/

WORKDIR ${JENKINS_HOME}

VOLUME ${JENKINS_HOME}

USER root

RUN apt-get update && \
    apt-get -y install \
               apt-transport-https \
               ca-certificates \
               curl \
               dnsutils \
               less \
               libssl-dev \
               libffi-dev \
               gnupg2 \
               mysql-client \
               netcat \
               net-tools \
               python \
               python-dev \
               python3-pip \
               python3-venv \
               redis-server \
               rsync \
               software-properties-common \
               telnet \
               traceroute \
               whois && \
    \
    # install nodejs version 16
    curl -fsSL https://deb.nodesource.com/setup_16.x | bash - && \
    apt-get install -y nodejs && \
    \
    # install venv and update default python version
    update-alternatives --install /usr/bin/python python /usr/bin/python2.7 1 && \
    update-alternatives --install /usr/bin/python python /usr/bin/python3.5 2 && \
    pip3 install \
          virtualenv \
          boto==2.48.0 && \
    \
    chmod 755 /usr/share/jenkins/ref/scriptApproval.xml && \
    \
    # clean up
    rm -rf /var/lib/apt/lists/*

FROM base as docker

ARG KUBECTL_VERSION=v1.19.14
ARG HELM_VERSION=v3.6.3

ENV KUBECTL_VERSION=${KUBECTL_VERSION} \
    HELM_VERSION=${HELM_VERSION}

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
    apt-get update && \
    apt-get -y install docker-ce=5:20.10.3~3-0~debian-buster && \
    usermod -a -G docker jenkins && \
    \
    # kubectl
    curl -LO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    chown root:docker /usr/local/bin/kubectl && \
    \
    # Install Helm
    curl -LO https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    tar -zxvf helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    chmod +x /usr/local/bin/helm

FROM docker as tools

ARG VAULT_VERSION=1.8.1
ARG EKSCTL_VERSION=0.36.0
ARG TERRAFORM_VERSION=0.15.5
ARG JQ_VERSION=1.6

ENV VAULT_VERSION=${VAULT_VERSION} \
    EKSCTL_VERSION=${EKSCTL_VERSION} \
    TERRAFORM_VERSION=${TERRAFORM_VERSION} \
    JQ_VERSION=${JQ_VERSION}

# AWS cli
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    \
    # eksctl
    curl -sLO https://github.com/weaveworks/eksctl/releases/download/${EKSCTL_VERSION}/eksctl_$(uname -s)_amd64.tar.gz && \
    tar -xvf eksctl_Linux_amd64.tar.gz && \
    mv eksctl /usr/local/bin && \
    \
    # jq
    curl -sLO https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 && \
    chmod +x jq-linux64 && \
    mv jq-linux64 /usr/bin/jq && \
    \
    # vault
    curl -sLO https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip && \
    unzip vault_${VAULT_VERSION}_linux_amd64.zip && \
    mv vault /usr/local/bin && \
    \
    # terraform
    curl -LO https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    mv terraform /usr/local/bin && \
    rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip

USER jenkins

COPY --chown=jenkins:jenkins plugins.txt /usr/share/jenkins/ref/plugins.txt

RUN jenkins-plugin-cli -f /usr/share/jenkins/ref/plugins.txt

# Jenkins Master

This is the Jenkins Master Docker Image. It is based off of the official jenkins
docker image. You can find it's [Docker Hub here](https://hub.docker.com/r/jenkins/jenkins)
and it's [Github here](https://github.com/jenkinsci/docker). It may periodically
need to be updated. Please check for depreciation warning on their respected `README.md`'s.

If you need to update the image to newer version please inspect the docker tags
in the dockerhub. It was chosen not to do the `lts` tag but rather the version
for clarity, verbosity, and sometimes the `lts` version is newer and we would
rather make the decision to manually control this in case of breaking changes
which may not be desired.

If you are updating this `Dockerfile` please also update any of those changes to
the jnlp-slave as well. As they are tightly coupled and most of the jobs are
going to be executed in the jnlp-slave pod.


## Build

There is a `Makefile` for convenience.

```bash
cd <this-dir>
make build
```

If you want to build a new version on the fly, you can

```bash
cd <this-dir>
make build JENKINS_VERSION=<MY-NEW-VERSION>
```

But please keep in mind. There are defaults set in the `Makefile` and
`Dockerfile`. If your new updated version is working and you wish to use it
moving forward, please update those defaults so that the next developer can also
build the correct version.


## Push

If everything looks ok with your new docker image and you are ready to push it
to ECR

```bash
cd <this-dir>
make push
```

If you get an error about not authorized to push to ecr

```bash
make login
```

Then try pushing again. Please check to see if your work is in ECR.


## Deploy

If you want to deploy jenkins using this new version you just built, you will
need to go to the helm directory.

```bash
cd </path/to/helm/jenkins>
make install
```

Please be courteous to your coworkers and communicate the changes you just did
and your intentions of redeploying the jenkins server to ensure you are not
disrupting their work.


# Jenkins on Kubernetes Boilerplate

My personal Jenkins on Kubernetes deploy from the official Helm chart and with
official docker images boilerplate.

I have grown tired of repeating myself, and finally reached a point where it is
wise to create this boilerplate repo so I can save myself time and avoid
repeating myself.

This boilerplate repo should have everything needed in order to build Jenkins
master and inbound agent along with some common and frequently used tools. It
should also have everything needed in order to deploy it to a kubernetes
cluster. And finally `Makefile`'s for everything! Yay!

I even included a few frequently used jenkins jobs that I will almost always use
for every jenkins setup.


## Docker

The jenkins master and inbound agent can be found under their respected folders
under the `docker` directory.


## Helm

You can find everything needed in order to deploy those custom built images to
your kubernetes cluster under the `helm` directory.


## Jobs

Under the `jobs` directory you can find some commonly used jenkins jobs.


## How to


### Build

To build the docker images. Once you have made your desired changes to the
`Dockerfile`'s under the `docker` folder. Updates the `Makefile`'s to tag and
push to the correct repositories. Maybe updated the installed tools versions,
etc.

Build Master

```bash
cd docker/jenkins/
make all
```

Build inbound-agent

```bash
cd docker/inbound-agent
make all
```


### Deploy

Once the images are built and pushed to your repositories. You can update the
helm chart to your liking. Make sure to update the helm `Makefile` so that it is
going to deploy the correct docker image tag and repositories. If it is a
private repository you will need to make sure you have the correct kubernetes
secret already deployed to your cluster.

```
cd helm/
make init
make pull
```

Make any changes to the values.yaml or using the one in this repository. This
`values.yaml` does expect external dns to be setup and running in order to
create the DNS record for the jenkins url.

Deploy with Helm

```bash
make install
```

And that's it. You should have jenkins up and running in your kubernetes
cluster. It should output some information about how to get the first one time
use admin password. Make sure you follow those instructions. After you have
logged in for the first time, it's time to set it up.


### Setup

Now that it's up and running, and you have logged in for the first time. You
probably want to set it up with github oauth so that you can grant everyone
access to the jenkins server through github.

Next you probably want to setup the jenkins backup job so that we don't loose
any of the work we are doing. Or even `velero` so that the `pvc` is getting backed
up.

After that, you will likely want to setup the kubernetes config so that you can
scale out each job with the inbound agents.

And that is pretty much it for what the boilerplate repo should contain.
Everything else will likely be too project specific.

Other things you probably want to setup right away, is some common credentials
such as the jenkins iam user/role AWS credentials.


### Updating

When you want to update jenkins version. Make sure you update the plugins first.
Next you will want to update the `Dockerfile` versions, and the `Makefile`
versions. Then re-build the master and inbound agents.

Then you can update the `Makefile` version in the helm chart directory, as well
as the new docker image tags for the master and inbound agent. Then you can run
a regular `make update` to deploy the new jenkins version.


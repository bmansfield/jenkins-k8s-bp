# Jenkins Helm Chart

This is based off the [official Jenkins Helm Chart](https://github.com/jenkinsci/helm-charts/tree/main/charts/jenkins)


## How To Use

I made a boilerplate to quickly get started on any project with just a few quick
strokes.

```bash
make init
```

This will pull and/or update the jenkins helm chart


### Deploy


#### Prerequisites

Before you can actually deploy, the way this chart is setup, you will need to
deploy a few secrets in order for this chart to work correctly. Unless you have
decided to not utilize some of the features which are set in the `values-override.yaml`.

AWS keys

```bash
kubectl create \
        secret \
        -n jenkins \
        generic \
        jenkinsaws \
        --from-literal=userAWS_ACCESS_KEY_ID="XXXXXXX" \
        --from-literal=AWS_SECRET_ACCESS_KEY="XXXXXXXXXXXXXXXX"
```

You will also want to create the jenkins aws account and S3 bucket if you want
to use the backup feature.

Jenkins Admin Password

```bash
kubectl create \
        secret \
        -n jenkins \
        generic \
        jenkins-admin \
        --from-literal=user="admin" \
        --from-literal=password="XXXXXXXXX"
```

Private Docker Registry

```bash
docker login
cat ~/.docker/config.json > dockerregconfig.json
kubectl create \
        secret \
        generic \
        docker-registry-credentials \
        --from-file=.dockerconfigjson=<path/to/dockerregconfig.json> \
        --type=kubernetes.io/dockerconfigjson
```

Now you should be able to deploy the chart.


#### Helm Deploy

Make your adjusted changes to the `values-override.yaml`.

```bash
make deploy
```


### Post Deploy

Once your jenkins server is running. There are a few typical tasks you usually
want to do. Login and update/add all of your plugins which you might need and
are not included in the helm values file.

Setup any folders or jobs. Test their github connection, and that things are
spinning up in jenkins inbound agent pods as expected.


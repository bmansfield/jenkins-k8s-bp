# Jenkins Jobs

Commonly used Jenkins Jobs that I do not wish to create from scratch anymore.


## More Jenkins Jobs

If you are looking for more Jenkins Jobs examples. I have one in my [boilerplate repository here](https://gitlab.com/bmansfield/bp-repo/-/blob/master/Jenkinsfile). Hopefully one day I can
get around to making a more official "Jenkinsfile" boilerplate repo with a list
of specific examples.

#!/usr/bin/env groovy

def COLOR_MAP = ['SUCCESS': 'good', 'FAILURE': 'danger', 'UNSTABLE': 'danger', 'ABORTED': 'danger']

def S3_URL = "s3://jenkins-backups/"

def getDate() {
  def date = sh(returnStdout: true, script: 'date +"%Y-%m-%d"').trim()
  return "${date}"
}

def getOldDate() {
  def date = sh(returnStdout: true, script: 'date -d "$date -14 days" +"%Y-%m-%d"').trim()
  return "${date}"
}

pipeline {

  agent {
    label 'master'
  }

  triggers {
    cron('0 9 * * *')
  }

  environment {
    DATE = getDate()
    OLD = getOldDate()
    WORK_DIR = "jenkins/jenkins-backup"
    AWS_ACCESS_KEY_ID = credentials('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = credentials('AWS_SECRET_ACCESS_KEY')
  }

  stages {

    stage('Backup') {

      steps {
        script {
          dir("${WORK_DIR}") {
            sh "./create_backup.sh"
            sh "aws s3 cp jenkins-backup-${DATE}.tar.gz ${S3_URL}"
            sh "rm -fr jenkins-backup-${DATE}.tar.gz"
          }
        }
      }

      post {
        failure {
          slackSend (
            color: COLOR_MAP[currentBuild.currentResult],
            channel: "devops",
            message: "*FAILED:* Jenkins backup failed. Please correct issue and run again - (${env.BUILD_URL})"
          )
        }
      }
    }

    stage('Remove Outdated Backup') {

      steps {
        sh "aws s3 rm ${S3_URL}/jenkins-backup-${OLD}.tar.gz"
      }

      post {
        failure {
          slackSend (
            color: COLOR_MAP[currentBuild.currentResult],
            channel: "devops",
            message: "*FAILED:* Jenkins failed to delete outdated backup. Please correct issue and run again - (${env.BUILD_URL})"
          )
        }
      }
    }
  }
}
